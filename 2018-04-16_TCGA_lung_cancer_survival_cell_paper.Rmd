---
title: "2018-04-16_TCGA_lung_survival"
author: "Ming Tang"
date: "April 16, 2018"
output: html_document
editor_options: 
  chunk_output_type: console
---

### use the most recent TCGA survival data 

The survival data was downloaded from https://xenabrowser.net/datapages/?hub=https://pancanatlas.xenahubs.net:443
TCGA published the pan-cancer studies http://www.cell.com/cell/fulltext/S0092-8674(18)30229-0

```{bash}
wget https://pancanatlas.xenahubs.net/download/Survival_SupplementalTable_S1_20171025_xena_sp
```





read in R
```{r}
library(tidyverse)
library(here)
library(janitor)
TCGA_survival<- read_tsv(here("data/TCGA_survival/Survival_SupplementalTable_S1_20171025_xena_sp"))

Lung_survival<- TCGA_survival %>%
  dplyr::filter(`cancer type abbreviation` %in% c("LUAD", "LUSC") ) %>%
  dplyr::select(sample, `cancer type abbreviation`, vital_status, OS, OS.time, DSS, DSS.time, DFI, DFI.time, PFI, PFI.time) %>%
  dplyr::rename(cancer_type = `cancer type abbreviation`)



```

OS (overall survival), DSS (disease-specific survival), DFI (Disease-free interval), PFI (progression-free interval), and PFS (Progression-free survival) https://www.synapse.org/#!Synapse:syn7343873/wiki/412112  

Recommended use of the endpoints: For clinical outcome endpoints, we recommend use of PFI for progression-free interval, OS for overall survival. Both endpoints are pretty accurate. Given the relatively short follow-up time, PFI is preferred over OS.

DFI is derived based on either "treatment_outcome_first_course" is "Complete Remission/Response" or "residual_tumor" is R0, or "margin_status" is negative. The endpoint is relatively accurate. For SKCM, THYM, UVM, Disease-Free Interval is not available.

DSS is relatively accurate for CESC, PAAD, and UVM, and is approximate for other tumor types.

### KMT2D truncating mutants vs all WT samples

```{r}

LUAD_KMT2D_WT_samples<- read_tsv("data/LUAD_KMT2D_WT_samples.txt", col_names = F) %>% 
  pull(X1) %>%
  str_sub(1,15)
  
LUSC_KMT2D_WT_samples<- read_tsv("data/LUSC_KMT2D_WT_samples.txt", col_names = F) %>% 
  pull(X1) %>%
  str_sub(1,15)

LUAD_KMT2D_nonsense_samples<- read_tsv("data/LUAD_KMT2D_nonsense_samples.txt", col_names = F) %>% 
  pull(X1) %>%
  str_sub(1,15)
LUSC_KMT2D_nonsense_samples<- read_tsv("data/LUSC_KMT2D_nonsense_samples.txt", col_names =F) %>% 
  pull(X1) %>%
  str_sub(1,15)

LUAD_KMT2D_mut_samples<- read_tsv("data/LUAD_KMT2D_mut_samples.txt", col_names =F) %>% 
  pull(X1) %>%
  str_sub(1,15)

LUSC_KMT2D_mut_samples<- read_tsv("data/LUSC_KMT2D_mut_samples.txt", col_name =F) %>%
  pull(X1) %>%
  str_sub(1,15)


Lung_KMT2D_status_survival<- Lung_survival %>% mutate(KMT2D_status = case_when(
  sample %in% c(LUAD_KMT2D_WT_samples, LUSC_KMT2D_WT_samples) ~ "WT",
  sample %in% c(LUAD_KMT2D_nonsense_samples, LUSC_KMT2D_nonsense_samples) ~ "nonsense"
)) %>%
  dplyr::filter(! is.na(KMT2D_status))

Lung_KMT2D_status_survival2<- Lung_survival %>% mutate(KMT2D_status = case_when(
  sample %in% c(LUAD_KMT2D_WT_samples, LUSC_KMT2D_WT_samples) ~"WT",
  sample %in% c(LUAD_KMT2D_nonsense_samples, LUSC_KMT2D_nonsense_samples) ~"nonsense"
)) %>% mutate(KMT2D_status2 = case_when(
  sample %in% c(LUAD_KMT2D_WT_samples, LUSC_KMT2D_WT_samples) ~"WT",
  sample %in% c(LUAD_KMT2D_mut_samples, LUSC_KMT2D_mut_samples) ~"mutated"
))

write_tsv(Lung_KMT2D_status_survival2, "results/Lung_KMT2D_status_survival_data.tsv")


table(Lung_KMT2D_status_survival$KMT2D_status)

Lung_KMT2D_status_survival %>%  tabyl(KMT2D_status,  cancer_type)
```


### survival analysis 

```{r}
library(survival)
library(broom)
LUAD_survfit<- survfit(Surv(time = PFI.time, event = PFI) ~ KMT2D_status, data = Lung_KMT2D_status_survival)
LUAD_tidy<- tidy(LUAD_survfit)

##logrank test, default
survdiff(Surv(time = PFI.time, event = PFI) ~ KMT2D_status, data =  Lung_KMT2D_status_survival, rho = 0)


#https://github.com/kassambara/survminer
library("survminer")

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           ggtheme = theme_gray(base_size=15))

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           )
```


```{r}

survdiff(Surv(time = new_death, event = death_event) ~ subtype, data = subset_clin, rho = 0) 
survdiff(Surv(time = new_death, event = death_event) ~ subtype, data = subset_clin, rho = 1) 

```

### KMT2D truncating mutants vs WT type high expression KMT2D


```{r}

load("data/TCGA_RNAseq/LUSCRnaseqSE.rda")
load("data/TCGA_RNAseq/LUADRnaseqSE.rda")

## raw counts
LUSCMatrix <- assay(LUSCRnaseqSE) 
## raw counts
LUADMatrix <- assay(LUADRnaseqSE) 

colnames(LUADMatrix)<- colnames(LUADMatrix) %>% str_sub(1, 15)
colnames(LUSCMatrix)<- colnames(LUSCMatrix) %>% str_sub(1, 15)

LUADMatrix_WT<- LUADMatrix[, colnames(LUADMatrix) %in% LUAD_KMT2D_WT_samples]
LUSCMatrix_WT<- LUSCMatrix[, colnames(LUSCMatrix) %in% LUSC_KMT2D_WT_samples]

LUADMatrix_nonsense<- LUADMatrix[, colnames(LUADMatrix) %in% LUAD_KMT2D_nonsense_samples]
LUSCMatrix_nonsense<- LUSCMatrix[, colnames(LUSCMatrix) %in% LUSC_KMT2D_nonsense_samples]

LUADMatrix_WT %>% head()

## convert to TPM
source("https://bioconductor.org/biocLite.R")
#biocLite("EnsDb.Hsapiens.v83")

#create a tx2gene.txt table
library(EnsDb.Hsapiens.v86)
edb <- EnsDb.Hsapiens.v86

genes_ensemble <- genes(edb)

gene_length<- as.data.frame(genes_ensemble) %>% dplyr::select(gene_id, gene_name, width)


gene_length_in_mat<- left_join(data.frame(gene_id = rownames(LUADMatrix_WT)), gene_length) %>% dplyr::filter(!is.na(width))

LUADMatrix_WT_mat_sub<- LUADMatrix_WT[rownames(LUADMatrix_WT) %in% gene_length_in_mat$gene_id, ]
LUSCMatrix_WT_mat_sub<- LUSCMatrix_WT[rownames(LUSCMatrix_WT) %in% gene_length_in_mat$gene_id, ]
## now, just use gene_length instead of effective length. effective length = gene length - fragment_length

all.equal(rownames(LUADMatrix_WT), gene_length_in_mat$gene_id)
all.equal(rownames(LUSCMatrix_WT), gene_length_in_mat$gene_id)

countToTpm <- function(counts, effLen)
{
    rate <- log(counts + 1) - log(effLen)
    denom <- log(sum(exp(rate)))
    exp(rate - denom + log(1e6))
}


LUADMatrix_WT_TPM<- apply(LUADMatrix_WT_mat_sub, 2, countToTpm, effLen = gene_length_in_mat$width)
LUSCMatrix_WT_TPM<- apply(LUSCMatrix_WT_mat_sub, 2, countToTpm, effLen = gene_length_in_mat$width)

head(LUADMatrix_WT_TPM)

hist(log2(LUADMatrix_WT_TPM))
hist(log2(LUSCMatrix_WT_TPM))

## top and low percentile

cutoffs<- 0.2

## only the samples have survival data. and remove the normal samples 

colnames(LUSCMatrix_WT_TPM) %>% str_sub(14, 16) %>% table()
## the samples are subseted by WT status of KMT2D, those are all tumors, no normals

## KMT2D
LUAD_gene_to_check<-  LUADMatrix_WT_TPM["ENSG00000167548", ]
LUSC_gene_to_check<-  LUSCMatrix_WT_TPM["ENSG00000167548", ]

Lung_KMT2D_status_survival %>%  tabyl(KMT2D_status,  cancer_type)

## choose 4 WT KMT2D high LUAD samples and 35 WT KMT2D high LUSC samples
LUAD_gene_to_check<- data.frame(sample = names(LUAD_gene_to_check), TPM = LUAD_gene_to_check)
LUSC_gene_to_check<- data.frame(sample = names(LUSC_gene_to_check), TPM = LUSC_gene_to_check)

Lung_gene_to_check<- rbind(LUAD_gene_to_check, LUSC_gene_to_check)

LUAD_WT_survival<- left_join(Lung_KMT2D_status_survival, Lung_gene_to_check) %>% 
 dplyr::filter(cancer_type == "LUAD", KMT2D_status == "WT") %>%
  top_n(4, TPM)

LUSC_WT_survival<- left_join(Lung_KMT2D_status_survival, Lung_gene_to_check) %>% 
 dplyr::filter(cancer_type == "LUSC", KMT2D_status == "WT") %>%
  top_n(35, TPM)

Lung_nonsense_survival<- left_join(Lung_KMT2D_status_survival, Lung_gene_to_check) %>% dplyr::filter(KMT2D_status == "nonsense")

Lung_nonsense_WT_same_number_survival<- rbind(LUAD_WT_survival, LUSC_WT_survival, Lung_nonsense_survival)

LUAD_WT_survival<- left_join(Lung_KMT2D_status_survival, Lung_gene_to_check) %>%
  dplyr::filter(cancer_type == "LUAD", KMT2D_status == "WT", !is.na(TPM))

cutoffs<- 0.2
qts<- quantile(LUAD_WT_survival$TPM, prob = c(cutoffs, 1- cutoffs))

LUAD_WT_survival_sub<- LUAD_WT_survival %>%
  mutate(subtype = case_when(
    TPM <= qts[1] ~ "low",
    TPM >= qts[2] ~ "high",
    TRUE ~ "medium"
  )) %>% 
  dplyr::filter(subtype != "medium")
```

### survival 

```{r}
library(survival)
library(broom)
Lung_survfit<- survfit(Surv(time = OS.time, event = OS) ~ KMT2D_status, data = Lung_nonsense_WT_same_number_survival)
Lung_tidy<- tidy(Lung_survfit)

##logrank test, default
survdiff(Surv(time = OS.time, event = OS) ~ KMT2D_status, data =  Lung_nonsense_WT_same_number_survival, rho = 0)


#https://github.com/kassambara/survminer
library("survminer")

ggsurvplot(Lung_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           ggtheme = theme_gray(base_size=15))

ggsurvplot(Lung_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           )


LUAD_survfit<- survfit(Surv(time = PFI.time, event = PFI) ~ subtype, data = LUAD_WT_survival_sub)
LUAD_tidy<- tidy(LUAD_survfit)

##logrank test, default
survdiff(Surv(time = OS.time, event = OS) ~ subtype, data =  LUAD_WT_survival_sub, rho = 0)


#https://github.com/kassambara/survminer
library("survminer")

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           ggtheme = theme_gray(base_size=15))

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE
           )
```

### KMT2D mut vs WT all

```{r}
LUAD_KMT2D_WT_samples<- read_tsv("data/LUAD_KMT2D_WT_samples.txt", col_names = F) %>% 
  pull(X1) %>%
  str_sub(1,15)
  
LUSC_KMT2D_WT_samples<- read_tsv("data/LUSC_KMT2D_WT_samples.txt", col_names = F) %>% 
  pull(X1) %>%
  str_sub(1,15)

LUAD_KMT2D_mut_samples<- read_tsv("data/LUAD_KMT2D_mut_samples.txt", col_names = F) %>% 
  pull(X1) %>%
  str_sub(1,15)
  
LUSC_KMT2D_mut_samples<- read_tsv("data/LUSC_KMT2D_mut_samples.txt", col_names = F) %>% 
  pull(X1) %>%
  str_sub(1,15)


Lung_KMT2D_mut_survival<- Lung_survival %>% mutate(KMT2D_status = case_when(
  sample %in% c(LUAD_KMT2D_WT_samples, LUSC_KMT2D_WT_samples) ~ "WT",
  sample %in% c(LUAD_KMT2D_mut_samples, LUSC_KMT2D_mut_samples) ~ "Mutant"
)) %>%
  dplyr::filter(! is.na(KMT2D_status))


table(Lung_KMT2D_mut_survival$KMT2D_status)

Lung_KMT2D_mut_survival %>%  tabyl(KMT2D_status,  cancer_type)
```

### survival analysis 

```{r}
library(survival)
library(broom)
LUAD_survfit<- survfit(Surv(time = PFI.time, event = PFI) ~ KMT2D_status, data = Lung_KMT2D_mut_survival %>% 
                         dplyr::filter(cancer_type == "LUAD"))
LUAD_tidy<- tidy(LUAD_survfit)

##logrank test, default
survdiff(Surv(time = PFI.time, event = PFI) ~ KMT2D_status, data =  Lung_KMT2D_status_survival, rho = 0)


#https://github.com/kassambara/survminer
library("survminer")

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           ggtheme = theme_gray(base_size=15))

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE
           )
```


### replicate the cell paper publication

```{r}
TCGA_survival<- dplyr::rename(TCGA_survival, cancer_type = `cancer type abbreviation` )
TCGA_survfit<- survfit(Surv(time = OS.time/365, event = OS) ~ cancer_type, data = TCGA_survival %>%
                         dplyr::filter(cancer_type %in% c("GBM", "TGCT", "SKCM", "MESO", "BRCA", "THCA")))




#https://github.com/kassambara/survminer
library("survminer")

ggsurvplot(TCGA_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE)
```
