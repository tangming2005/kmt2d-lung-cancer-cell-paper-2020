### make a meta file for DiffBind

```bash
cd /scratch/genomic_med/mtang1/scratch/UCI-hunain

paste <(find . -name "*H3K27Ac.sorted.bam"| sort) <(find . -name "*Input.sorted.bam" | sort) <(find . -name "*h3k27ac_*SuperEnhancers.bed" | sort) | grep -v "KD_" | tee hunai_H3K27ac_super_meta_file.txt 

paste <(find . -name "*H3K4me1.sorted.bam"| sort) <(find . -name "*Input.sorted.bam" | sort) <(find . -name "*h3k4me1_*SuperEnhancers.bed" | sort) | grep -v "KD_" | tee hunai_H3K4me1_super_meta_file.txt

```

mannually correct the pairing of the bed files.

### super-enhancer diffbind

```{r}
library(readr)
library(dplyr)

UCI_H3K27ac_super_meta<- read_tsv("/Volumes/mdarisngc04.mdanderson.edu/scratch/UCI-hunain/hunai_H3K27ac_super_meta_file.txt", col_names = F)

UCI_H3K4me1_super_meta<- read_tsv("/Volumes/mdarisngc04.mdanderson.edu/scratch/UCI-hunain/hunai_H3K4me1_super_meta_file.txt", col_names = F)

names(UCI_H3K27ac_super_meta)<- c("bamReads", "bamControl", "Peaks")
names(UCI_H3K4me1_super_meta)<- c("bamReads", "bamControl", "Peaks")



UCI_H3K27ac_super_meta$SampleID<- c("MLL_1576", "RAS_1475", "RAS_1508", "MLL_1527")
UCI_H3K4me1_super_meta$SampleID<- c("MLL_1576", "RAS_1475", "RAS_1508", "MLL_1527")


UCI_H3K27ac_super_diffbind<- UCI_H3K27ac_super_meta %>% 
        mutate(ControlID = paste0(SampleID,"c")) %>% 
        mutate(PeakCaller = "raw", Replicate = c(1,1,2,2)) %>% 
        mutate(Condition = c("KO", "WT", "WT","KO")) %>% 
        dplyr::select(SampleID, Condition, Replicate, bamReads, ControlID, bamControl, Peaks, PeakCaller)


UCI_H3K4me1_super_diffbind<- UCI_H3K4me1_super_meta %>% 
        mutate(ControlID = paste0(SampleID,"c")) %>% 
        mutate(PeakCaller = "raw", Replicate = c(1,1,2,2)) %>% 
        mutate(Condition = c("KO", "WT", "WT","KO")) %>% 
        dplyr::select(SampleID, Condition, Replicate, bamReads, ControlID, bamControl, Peaks, PeakCaller)



write.table(UCI_H3K27ac_super_diffbind, "/Volumes/mdarisngc04.mdanderson.edu/scratch/UCI-hunain/H3K27ac_super_diffbind.csv", 
            sep=",", quote=T, col.names = TRUE,
            row.names = F)

write.table(UCI_H3K4me1_super_diffbind, "/Volumes/mdarisngc04.mdanderson.edu/scratch/UCI-hunain/H3K4me1_super_diffbind.csv", 
            sep=",", quote=T, col.names = TRUE,
            row.names = F)

```

```{r}
library(DiffBind)
UCI.H3K27ac.super.dba<- dba(sampleSheet="H3K27ac_super_diffbind.csv")

UCI.H3K4me1.super.dba<- dba(sampleSheet="H3K4me1_super_diffbind.csv")


UCI_H3K27ac_super_RPKM<- dba.count(UCI.H3K27ac.super.dba, minOverlap=2, 
                      fragmentSize = 200, bParallel = T,
                      score = DBA_SCORE_RPKM)

UCI_H3K4me1_super_RPKM<- dba.count(UCI.H3K4me1.super.dba, minOverlap=2, 
                      fragmentSize = 200, bParallel = T,
                      score = DBA_SCORE_RPKM)


UCI_H3K27ac_super <- dba.contrast(UCI_H3K27ac_super_RPKM, categories=DBA_CONDITION, minMembers = 2)
UCI_H3K4me1_super <- dba.contrast(UCI_H3K4me1_super_RPKM, categories=DBA_CONDITION, minMembers = 2)


UCI_H3K27ac_super<- dba.analyze(UCI_H3K27ac_super)
UCI_H3K4me1_super<- dba.analyze(UCI_H3K4me1_super)

## retrive the diff bind sites, all sites
UCI_H3K27ac_super.DB <- dba.report(UCI_H3K27ac_super, th =1, bCounts = TRUE)
UCI_H3K4me1_super.DB <- dba.report(UCI_H3K4me1_super, th =1, bCounts = TRUE)


save(UCI_H3K27ac_super, UCI_H3K27ac_super.DB, UCI.H3K27ac.super.dba, UCI_H3K27ac_super_RPKM, file = "UCI_H3K27ac_super_diffbind.rda")

save(UCI_H3K4me1_super, UCI_H3K4me1_super.DB, UCI.H3K4me1.super.dba, UCI_H3K4me1_super_RPKM, file = "UCI_H3K4me1_super_diffbind.rda")
```

---
title: "cd45-diffbind_superEnhancer"
output: html_document
---
```{r}
library(DiffBind)
load("~/projects/Hunai_RNAseq/results/diffbind/UCI_H3K27ac_super_diffbind.rda")
load("~/projects/Hunai_RNAseq/results/diffbind/UCI_H3K4me1_super_diffbind.rda")
plot(UCI_H3K27ac_super)
plot(UCI_H3K4me1_super)

dba.plotPCA(UCI_H3K27ac_super,DBA_CONDITION,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K4me1_super,DBA_CONDITION,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K27ac_super, contrast=1,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K4me1_super, contrast=1,label=DBA_CONDITION)

### annotate with ChIPseeker
library(ChIPseeker)
UCI_H3K4me1_super.DB
UCI_H3K27ac_super.DB


library(TxDb.Mmusculus.UCSC.mm9.knownGene)
library(org.Mm.eg.db)
txdb <- TxDb.Mmusculus.UCSC.mm9.knownGene
H3K27ac_diff_sites_super_anno <- annotatePeak(UCI_H3K27ac_super.DB, tssRegion=c(-3000, 3000), 
                         TxDb=txdb, annoDb="org.Mm.eg.db", level = "gene", overlap = "all")

H3K4me1_diff_sites_super_anno <- annotatePeak(UCI_H3K4me1_super.DB, tssRegion=c(-3000, 3000), 
                         TxDb=txdb, annoDb="org.Mm.eg.db", level = "gene", overlap = "all")

View(H3K27ac_diff_sites_super_anno)

```

### intersect ChIP-seq and RNAseq data

```{r}


H3K27ac_super_rnaseq_merge<- left_join(as.data.frame(H3K27ac_diff_sites_super_anno), res_df, by = c("SYMBOL" = "symbol"))
H3K4me1_super_rnaseq_merge<- left_join(as.data.frame(H3K4me1_diff_sites_super_anno), res_df, by = c("SYMBOL" = "symbol"))

write_tsv(H3K27ac_super_rnaseq_merge, "~/projects/Hunai_RNAseq/results/diffbind/H3K27ac_super_rnaseq_merge.tsv")
write_tsv(H3K4me1_super_rnaseq_merge, "~/projects/Hunai_RNAseq/results/diffbind/H3K4me1_super_rnaseq_merge.tsv")


H3K27ac_super_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj <0.1) %>% arrange(desc(abs(Fold))) %>% head()

H3K27ac_super_rnaseq_merge %>% 
  filter(padj < 0.1) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + 
  geom_point() +
  geom_text_repel(data= dplyr::filter(H3K27ac_super_rnaseq_merge, padj < 0.1), aes(label=SYMBOL)) +
                theme_bw(base_size = 14)

H3K4me1_super_rnaseq_merge %>% 
  filter(padj < 0.1) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + 
  geom_point() +
  geom_text_repel(data= dplyr::filter(H3K4me1_super_rnaseq_merge, padj < 0.1 ), aes(label=SYMBOL)) +
                theme_bw(base_size = 14)



H3K27ac_super_down_peaks<- H3K27ac_super_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1 ) %>% select(seqnames, start, end) %>% makeGRangesFromDataFrame()

H3K4me1_super_down_peaks<- H3K4me1_super_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1 ) %>% select(seqnames, start, end) %>% makeGRangesFromDataFrame()

overlap_super.hits<- findOverlaps(H3K27ac_super_down_peaks, H3K4me1_super_down_peaks)
both_H3K27ac_H3K4me1_super_down<- H3K27ac_super_down_peaks[queryHits(overlap_super.hits)] %>% as.data.frame() %>% left_join(H3K27ac_super_rnaseq_merge)

write_tsv(both_H3K27ac_H3K4me1_super_down, "~/projects/Hunai_RNAseq/results/both_H3K27ac_H3K4me1_super_down.tsv")
```


