---
title: "UCI-hunai-intergrate-RNAseq"
author: "Ming Tang"
date: "January 19, 2017"
output: html_document
---

### use ggplot2 for volcano plot

```{r}
toptable_mll4<- read.table("~/playground/hunai/RNAseq/totalRNA/results_uci_rnaseq_wald_mll4Vsbg_results.tsv", sep = "\t", stringsAsFactors = F, header =T)

rld<- read.table("~/playground/hunai/RNAseq/totalRNA/results_uci_rnaseq_rld_dataframe.tsv", sep = "\t", stringsAsFactors = F, header = T)

library(dplyr)
library(ggplot2)
library(ggrepel)

toptable_mll4$Symbol<- rownames(toptable_mll4)

## some weired genes padj is NA
toptable_mll4<- filter(toptable_mll4, !is.na(padj))

plot_volcano<- function(toptable){
        results<- as.data.frame(toptable)
        results<- mutate(results, sig=ifelse(padj< 0.10, "FDR<0.10", "Not Sig"))

        p<-ggplot(results, aes(log2FoldChange, -log10(pvalue))) +
                geom_point(aes(col=sig)) +
                scale_color_manual(values=c("red", "black"))

        p<- p+ geom_text_repel(data= dplyr::filter(results, padj < 0.10), aes(label=Symbol)) +
                theme_bw(base_size = 14)
        print (p)

}

plot_volcano(toptable_mll4)

```

### remove low count genes

```{r}
rld<- rld %>% dplyr::select(-X1.Kdm2a.RasG12D.1079_htseq, -X2.Kdm2a.RasG12D.1031_htseq)

colnames(rld)<- c("MLL_1527", "MLL_1576", "RAS_1475", "RAS_1508", "ensembl_id", "entrez")

rld_mat<- as.matrix(rld[,1:4])

## at least 2 samples has log2 count greater than 2
rld_mat<- rld_mat[rowSums(rld_mat > 2) >=2,]

head(rld_mat)

selected_genes<- toptable_mll4 %>% filter(abs(log2FoldChange) >=1) %>% arrange(padj, desc(log2FoldChange)) %>% head(n=100) %>% .$Symbol

rld_sub<- rld_mat[rownames(rld_mat) %in% selected_genes,]

ht_global_opt(RESET = TRUE)

rld_mat_scale<- t(scale(t(rld_sub), center = T, scale= FALSE ))
Heatmap(rld_mat_scale, row_names_gp = gpar(fontsize = 6),
        name = "scaled gene\nexpression",
        column_title = "Heatmap of Mll4-KO vs Mll4-WT",
        cluster_columns = FALSE,
        clustering_distance_rows = "euclidean",
        clustering_method_rows = "complete",
        show_row_names = TRUE, show_column_names = TRUE, 
        row_dend_reorder = TRUE, column_dend_reorder = TRUE)
```

### integrate ChIP-seq and RNAseq

```{r}
library(TxDb.Mmusculus.UCSC.mm9.knownGene)
library(org.Mm.eg.db)
mm9_txdb<- TxDb.Mmusculus.UCSC.mm9.knownGene
mm9_genes<- genes(mm9_txdb)

mm9_symbol<- AnnotationDbi::select(org.Mm.eg.db, keys=mm9_genes$gene_id, 
                                    columns="SYMBOL", keytype="ENTREZID")

mm9_genes$symbol<- mm9_symbol$SYMBOL

## only select genes in RNAseq
mm9_genes_RNAseq<- mm9_genes[mm9_genes$symbol %in% rownames(rld_mat)]

## get the promoter region 5kb 

mm9_promoters<- promoters(mm9_genes_RNAseq, upstream = 5000, downstream = 5000)
mm9_promoters<- keepStandardChromosomes(mm9_promoters)
```

### read in the bigwig and quanitfy ChIP-seq intensity at promoters and enhancers.

```{r}

H3K4me1_bed<- import("peaks/H3K4me1_10_fold_peaks.bed", format = "BED")


H3K4me1_enhancers<- subsetByOverlaps(H3K4me1_all)
        
mm9_tss<- resize(mm9_promoters, width =1, fix = "center")  

## reorder tss the same as the expression matrix
mm9_tss<- mm9_tss[mm9_tss$symbol %in%  rownames(rld_mat_scale)]
 
mm9_tss<- mm9_tss[match(rownames(rld_mat_scale), mm9_tss$symbol)]

mm9_tss_5kb<- resize(mm9_tss, width = 10000, fix = "center")

library(rtracklayer)
RAS_1475.H3K4me1<- import("bigwigs/RAS_1475-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
RAS_1508.H3K4me1<- import("bigwigs/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
MLL_1576.H3K4me1<- import("bigwigs/MLL_1576-H3K4me1.sorted.bw", format= "bigWig", which = H3K4me1.10kb)
MLL_1527.H3K4me1<- import("bigwigs/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)

RAS_1475.H3K27ac<- import("bigwigs/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig", which = mm9_tss_5kb)
RAS_1508.H3K27ac<- import("bigwigs/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig", which = mm9_tss_5kb)
MLL_1576.H3K27ac<- import("bigwigs/MLL_1576-H3K27Ac.sorted.bw", format= "bigWig", which = mm9_tss_5kb)
MLL_1527.H3K27ac<- import("bigwigs/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig", which = mm9_tss_5kb)


library(EnrichedHeatmap)
RAS_1475.mat2<- normalizeToMatrix(RAS_1475.H3K27ac, mm9_tss, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat2<- normalizeToMatrix(RAS_1508.H3K27ac, mm9_tss, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat2<-normalizeToMatrix(MLL_1576.H3K27ac, mm9_tss, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat2<-normalizeToMatrix(MLL_1527.H3K27ac, mm9_tss, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

quantile(RAS_1475.mat2, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat2, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 40), c("white", "red"))

rld_mat_scale<- rld_mat_scale[,c(3,4,2,1)]
Heatmap(rld_mat_scale, row_names_gp = gpar(fontsize = 6),
        name = "scaled gene\nexpression",
        column_title = "RNAseq",
        cluster_columns = FALSE,
        clustering_distance_rows = "euclidean",
        clustering_method_rows = "complete",
        show_row_names = TRUE, show_column_names = TRUE, 
        row_dend_reorder = TRUE, column_dend_reorder = TRUE, width = 4) +
        EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 0, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "TSS", "5kb"), width = 1) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 0, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "TSS", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 0, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "TSS", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 0, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "TSS", "5kb"), width = 1) 

```