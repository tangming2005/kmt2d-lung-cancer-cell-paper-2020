---
title: "2017-11-1-chromHMM"
author: "Ming Tang"
date: "November 1, 2017"
output: html_document
---

```{r}
library(tidyverse)
library(here)
emission<- read_tsv(here("data/chromHMM/emissions_10.txt"), col_names = T)

emission_mat<- as.matrix(emission[, -1])
emission_mat<- emission_mat[, c(2,3,4,1,6,5)]
rownames(emission_mat)<- emission$`state (Emission order)`
colnames(emission_mat)<- c("H3K4me1", "H3K27ac", "H3K4me3", "H3K79me2", "H3K27me3", "H3K9me3")

library(ComplexHeatmap)

cell_fun = function(j, i, x, y, width, height, fill) {
	grid.rect(x = x, y = y, width = width *0.98, height = height *0.98, 
		gp = gpar(col = "grey", fill = fill, lty = 1, lwd = 0.5))
}


col_fun1<- circlize::colorRamp2(c(0, 1), c("white", "blue"))
pdf("results/emission.pdf", 4, 5)
Heatmap(emission_mat, cluster_rows = F, cluster_columns = F, show_row_names = T, show_column_names = T,
        col = col_fun1, rect_gp = gpar(type = "none"),
        cell_fun = cell_fun,
        name = "enrichment\nscore",
        column_title = "ChromHMM emission",
        heatmap_legend_param = list(color_bar = "discrete"))
dev.off()

pdf("results/emission_red.pdf", 4, 5)
col_fun2<- circlize::colorRamp2(c(0, 1), c("white", "red"))
Heatmap(emission_mat, cluster_rows = F, cluster_columns = F, show_row_names = T, show_column_names = T,
        col = col_fun2, rect_gp = gpar(type = "none"),
        cell_fun = cell_fun,
        name = "enrichment\nscore",
        column_title = "ChromHMM transition",
        heatmap_legend_param = list(color_bar = "discrete"))

dev.off()
```


overlap enrichment analysis 

```{bash}
## from MLL KO to WT
mkdir MLL_1527_segments
cd MLL_1527_segments
awk -F '\t' '{print >> $4;close($4)}' ../MLL_1527_10_segments.bed

mkdir MLL_1576_segments
cd MLL_1576_segments
awk -F '\t' '{print >> $4;close($4)}' ../MLL_1576_10_segments.bed

## change the name from E1 to E01, E2 to E02 etc
brename -p "E([1-9]$)" -r "E0\$1" 

java -mx12000M -jar /scratch/genomic_med/apps/chromhmm/chromhmm_v1.11/ChromHMM.jar OverlapEnrichment RAS_1475_10_segments.bed MLL_1527_segments RAS_1475_vs_MLL_1527

java -mx12000M -jar /scratch/genomic_med/apps/chromhmm/chromhmm_v1.11/ChromHMM.jar OverlapEnrichment RAS_1475_10_segments.bed MLL_1576_segments RAS_1475_vs_MLL_1576

java -mx12000M -jar /scratch/genomic_med/apps/chromhmm/chromhmm_v1.11/ChromHMM.jar OverlapEnrichment RAS_1508_10_segments.bed MLL_1527_segments RAS_1508_vs_MLL_1527

java -mx12000M -jar /scratch/genomic_med/apps/chromhmm/chromhmm_v1.11/ChromHMM.jar OverlapEnrichment RAS_1508_10_segments.bed MLL_1576_segments RAS_1508_vs_MLL_1576
```


transition heatmap

```{r}
transition<- read_tsv("data/chromHMM/RAS_1508_vs_MLL_1576.txt", col_names = T)
transition<- transition[, -2] %>% dplyr::rename(state = `state (Emission order)`) %>%
        filter(state != "Base")

transition_mat<- as.matrix(transition[,-1])
rownames(transition_mat)<- c("E01", "E02", "E03", "E04", "E05", "E06", "E07", "E08", "E09", "E10")

col_fun3<- circlize::colorRamp2(c(0, 3), c("white", "blue"))

## scale the transition matrix by column 
transition_mat_scale<- scale(transition_mat, center = T, scale = T)

pdf("results/transition_RAS1508_vs_MLL1576.pdf", 4, 4)
Heatmap(transition_mat_scale, cluster_rows = F, cluster_columns = F, show_row_names = T, show_column_names = T,
        col = col_fun3, rect_gp = gpar(type = "none"),
        cell_fun = cell_fun,
        name = "Fold\nenrichment",
        column_title = "ChromHMM transition",
        heatmap_legend_param = list(color_bar = "discrete"))
dev.off()

pdf("results/transition_RAS1508_vs_MLL1576_red.pdf", 4, 4)
col_fun4<- circlize::colorRamp2(c(0, 3), c("white", "red"))
Heatmap(transition_mat_scale, cluster_rows = F, cluster_columns = F, show_row_names = T, show_column_names = T,
        col = col_fun4, rect_gp = gpar(type = "none"),
        cell_fun = cell_fun,
        name = "Fold\nenrichment",
        column_title = "ChromHMM transition",
        heatmap_legend_param = list(color_bar = "discrete"))
dev.off()
```