---
title: "01_cd45_mouse_RNAseq"
author: "Ming Tang"
date: "December 1, 2017"
output: github_document
editor_options: 
  chunk_output_type: console
---

```{r}
library(DESeq2)
library(tidyverse)
library(here)
samples<- c("Kdm2a_1291", "Kdm2a_1294", "Mll4_1612", "Mll4_1646", "WT_1665", "WT_1507")
condition<- c("Kdm2a", "Kdm2a", "Mll4", "Mll4", "WT", "WT")
meta<- data.frame(condition = condition)
rownames(meta)<- samples

directory <- here("data/cd45_RNAseq")

sampleFiles <- list.files(directory, pattern = ".cnt")
sampleCondition <- c("Kdm2a", "Kdm2a", "Mll4", "Mll4", "WT", "WT")
sampleTable <- data.frame(sampleName = samples,
                          fileName = sampleFiles,
                          condition = sampleCondition)

ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable,
                                       directory = directory,
                                       design= ~ condition)
ddsHTSeq


## pre-filter the low count genes
dds <- ddsHTSeq[ rowSums(counts(ddsHTSeq)) > 1, ]

dds$condition <- relevel(dds$condition, ref="WT")

dds <- DESeq(dds)

resultsNames(dds)

## specify contrast
res<- results(dds, contrast=c("condition","Mll4","WT"))

mll4_KO_pre_rank<- sign(res$log2FoldChange) * -log10(res$pvalue)

write_tsv(data.frame(Name = res$symbol, metric = mll4_KO_pre_rank) %>% na.omit(), "results/mll4_KO_vs_WT_pre_rank.rnk")


res$symbol<- rownames(res)
hist(res$pvalue)

as.data.frame(res) %>% filter(symbol == "Arid3a")
as.data.frame(res) %>% filter(symbol == "Casz1")
as.data.frame(res) %>% filter(symbol == "Epas1")
as.data.frame(res) %>% filter(symbol == "Casz1")
as.data.frame(res) %>% filter(symbol == "Mkl2")
as.data.frame(res) %>% filter(symbol == "Nfic")
as.data.frame(res) %>% filter(symbol == "Per2")
as.data.frame(res) %>% filter(symbol == "Tns2")
as.data.frame(res) %>% filter(symbol == "Sox6")
as.data.frame(res) %>% filter(symbol == "Tal1")
as.data.frame(res) %>% filter(symbol == "Tbx4")

as.data.frame(res) %>% filter(symbol == "Cdk1")
as.data.frame(res) %>% filter(symbol == "Tbx4")
as.data.frame(res) %>% filter(symbol == "Sox9")
as.data.frame(res) %>% filter(symbol == "Tp63")



write_tsv(as.data.frame(res), "results/mll4_vs_wt_htseq_DESeq2.tsv")
res_df<- as.data.frame(res)
res_df<- filter(res_df, !is.na(padj))

library(ggrepel)
plot_volcano<- function(toptable){
        results<- as.data.frame(toptable)
        results<- mutate(results, sig=ifelse(padj< 0.05, "FDR<0.05", "Not Sig"))

        p<-ggplot(results, aes(log2FoldChange, -log10(pvalue))) +
                geom_point(aes(col=sig)) +
                scale_color_manual(values=c("red", "black")) +
                ylim(c(0,50))

        p<- p+ geom_text_repel(data= dplyr::filter(results, padj < 0.01, abs(log2FoldChange) > 2.5), aes(label=symbol)) +
                theme_bw(base_size = 14)
        print (p)

}

plot_volcano(res_df)

```

### convert raw counts to TPM for comparison

```{r}
countToTpm <- function(counts, effLen)
{
    rate <- log(counts) - log(effLen)
    denom <- log(sum(exp(rate)))
    exp(rate - denom + log(1e6))
}

countToFpkm <- function(counts, effLen)
{
    N <- sum(counts)
    exp( log(counts) + log(1e9) - log(effLen) - log(N) )
}

library(TxDb.Mmusculus.UCSC.mm9.knownGene)
library(org.Mm.eg.db)
txdb <- TxDb.Mmusculus.UCSC.mm9.knownGene
mm9_genes<- genes(txdb)

library("org.Mm.eg.db")

## note that dplyr and AnnotationDbi both have a function called select
## use dplyr::select when use dplyr

gene_symbol<- AnnotationDbi::select(org.Mm.eg.db, keys=mm9_genes$gene_id, 
                                    columns="SYMBOL", keytype="ENTREZID")

all.equal(mm9_genes$gene_id, gene_symbol$ENTREZID)

mm9_genes$gene_id<- gene_symbol$SYMBOL

width(mm9_genes)


raw_counts<- counts(dds)


which(rownames(raw_counts) %in% mm9_genes$gene_id)

mm9_genes_sub<- mm9_genes[mm9_genes$gene_id %in% rownames(raw_counts)]

raw_counts_sub<- raw_counts[mm9_genes_sub$gene_id, ]

mll4_tpm<- apply(raw_counts_sub,  2, countToTpm, effLen =  width(mm9_genes_sub))


mll4_tpm_df<- as.data.frame(mll4_tpm)
mll4_tpm_df$gene_symbol<- rownames(mll4_tpm)

tpm_and_DEseq2_diff<- inner_join(as.data.frame(res), mll4_tpm_df, by = c("symbol" = "gene_symbol"))

tpm_and_DEseq2_diff %>% filter(symbol == "Tns2")
tpm_and_DEseq2_diff %>% filter(symbol == "Per2")
tpm_and_DEseq2_diff %>% filter(symbol == "Shank2")

write_tsv(tpm_and_DEseq2_diff, "results/mll4_vs_WT_DESeq2_and_tpm.tsv")
```

### rlog counts

```{r}

# rlog transformation
rld <- rlog(dds, blind=FALSE)
vsd <- varianceStabilizingTransformation(dds, blind=FALSE)
vsd.fast <- vst(dds, blind=FALSE)
head(assay(rld)[rownames(assay(rld)) != "", ])

plotPCA(rld, intgroup="condition")

## this will be used for GSEA analysis.
write.table(assay(rld)[rownames(assay(rld)) != "",3:6], "~/projects/Hunai_RNAseq/results/STAT-htseq/DESeq2/cd45_rld_htseq_DEseq2.tsv", col.names = T, row.names =T, quote = F, sep ='\t')

```