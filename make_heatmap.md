```bash

##4481 unique peaks in WT that are lost in MLL KO
bedtools intersect -a RAS_1475-H3K4me3_peaks.bed -b MLL_1576-H3K4me3_peaks.bed -v  | sort | uniq > RAS_1475_not_MLL_1576_H3K4me3_peaks.bed

## randome 4481 peaks which are present in both
bedtools intersect -a RAS_1475-H3K4me3_peaks.bed -b RAS_1475_not_MLL_1576_H3K4me3_peaks.bed  -v | sort | uniq | gshuf | head -4481 > RAS_1475_and_MLL_1576_H3K4me3.bed
 
cat RAS_1475-H3K4me1_peaks.xls | grep -v "#" | sed '1,2d' | awk '$8 >10' | cut -f1-3 > H3K4me1_10_fold_peaks.bed

 
 
```


```{r}

library(rtracklayer)
setwd("/Users/mtang1/playground/hunai")
H3K4me3_bed1<- import("peaks/RAS_1475_not_MLL_1576_H3K4me3_peaks.bed", format = "BED")
H3K4me3_bed2<- import("peaks/RAS_1475_and_MLL_1576_H3K4me3.bed", format = "BED")


H3K4me3.10kb<- resize(c(H3K4me3_bed1, H3K4me3_bed2), width = 20000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K4me3<- import("bigwigs/RAS_1475-H3K4me3.sorted.bw", format = "bigWig", which = H3K4me3.10kb)
RAS_1508.H3K4me3<- import("bigwigs/RAS_1508-1-H3K4me3.sorted.bw", format = "bigWig", which = H3K4me3.10kb)
MLL_1576.H3K4me3<- import("bigwigs/MLL_1576-H3K4me3.sorted.bw", format= "bigWig", which = H3K4me3.10kb)

library(EnrichedHeatmap)
H3K4me3.center<- resize(H3K4me3.10kb, width =1, fix = "center")

RAS_1475.mat<- normalizeToMatrix(RAS_1475.H3K4me3, H3K4me3.center, value_column = "score",
                           mean_mode="w0", w=100, extend = 10000)
RAS_1508.mat<- normalizeToMatrix(RAS_1508.H3K4me3, H3K4me3.center, value_column = "score",
                           mean_mode="w0", w=100, extend = 10000)
MLL_1576.mat<-normalizeToMatrix(MLL_1576.H3K4me3, H3K4me3.center, value_column = "score",
                            mean_mode="w0", w=100, extend = 10000)

set.seed(123)
EnrichedHeatmap(RAS_1475.mat, axis_name_rot = 0, name = "RAS_1475 H3K4me3",
                column_title = "RAS_1475", use_raster = TRUE, row_order = 1:691) +
        EnrichedHeatmap(RAS_1508.mat, axis_name_rot = 0, name = "RAS_1508 H3K4me3", 
                        column_title = "RAS_1508", use_raster = TRUE) +
        EnrichedHeatmap(MLL_1576.mat, axis_name_rot = 0, name = "MLL_1576 H3K4me3", 
                        column_title ="MLL_1576", use_raster = TRUE) 

### mapping colors

library(circlize)
quantile(RAS_1475.mat, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat, probs = c(0.005, 0.5,0.995))

col_fun<- circlize::colorRamp2(c(0, 150), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat, axis_name_rot = 0, name = "RAS_1475 H3K4me3",
                column_title = "RAS_1475", use_raster = TRUE, 
                row_order = 1:8962, col= col_fun, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(RAS_1508.mat, axis_name_rot = 0, name = "RAS_1508 H3K4me3", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(MLL_1576.mat, axis_name_rot = 0, name = "MLL_1576 H3K4me3", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun, pos_line =F, axis_name_gp = gpar(fontsize = 14)) 

EnrichedHeatmap(mets.mat, axis_name_rot = 0, name = "mets H3K4me3",
                column_title = "mets", use_raster = TRUE, 
                row_order = 1:691, col= col_fun, split = c(rep("group1",250), rep("group2",441))) +
        EnrichedHeatmap(T4.mat, axis_name_rot = 0, name = "T4 H3K4me3", 
                        column_title ="T4", use_raster = TRUE, col= col_fun) +
        EnrichedHeatmap(T1.mat, axis_name_rot = 0, name = "T1 H3K4me3", 
                        column_title = "T1", use_raster = TRUE, col= col_fun) 



Heatmap(as.matrix(mets.met),  name = "mets H3K4me3", 
        column_title = "mets", use_raster = TRUE )

attributes(T1.mat)

###########

H3K4me1_bed<- import("peaks/H3K4me1_10_fold_peaks.bed", format = "BED")
H3K4me1.10kb<- resize(H3K4me1_bed, width = 20000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K4me1<- import("bigwigs/RAS_1475-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
RAS_1508.H3K4me1<- import("bigwigs/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)
MLL_1576.H3K4me1<- import("bigwigs/MLL_1576-H3K4me1.sorted.bw", format= "bigWig", which = H3K4me1.10kb)
MLL_1527.H3K4me1<- import("bigwigs/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.10kb)

library(EnrichedHeatmap)
H3K4me1.center<- resize(H3K4me1.10kb, width =1, fix = "center")

RAS_1475.mat1<- normalizeToMatrix(RAS_1475.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 10000)
RAS_1508.mat1<- normalizeToMatrix(RAS_1508.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 10000)
MLL_1576.mat1<-normalizeToMatrix(MLL_1576.H3K4me1, H3K4me1.center, value_column = "score",
                                mean_mode="w0", w=100, extend = 10000)

MLL_1527.mat1<-normalizeToMatrix(MLL_1527.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 10000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat1, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat1, probs = c(0.005, 0.5,0.995))

col_fun1<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat1, axis_name_rot = 0, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(RAS_1508.mat1, axis_name_rot = 0, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(MLL_1576.mat1, axis_name_rot = 0, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(MLL_1527.mat1, axis_name_rot = 0, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) 
        
        


```